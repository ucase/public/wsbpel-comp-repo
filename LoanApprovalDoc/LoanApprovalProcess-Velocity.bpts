<?xml version="1.0" encoding="UTF-8"?>
<!--
   Copyright (C) 2010 Antonio García Domínguez

   This file is part of the LoanApprovalVelocity composition in the UCASE
   WS-BPEL composition repository.

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<tes:testSuite
    xmlns:esq="http://xml.netbeans.org/schema/Loans"
    xmlns:ap="http://j2ee.netbeans.org/wsdl/ApprovalService"
    xmlns:as="http://j2ee.netbeans.org/wsdl/AssessorService"
    xmlns:sp="http://j2ee.netbeans.org/wsdl/LoanService"
    xmlns:pr="http://enterprise.netbeans.org/bpel/N6_ServicioPrestamo/LoanApprovalProcess"
    xmlns:tes="http://www.bpelunit.org/schema/testSuite">

  <tes:name>LoanServiceTest</tes:name>
  <tes:baseURL>http://localhost:7777/ws</tes:baseURL>

  <tes:deployment>
    <tes:put name="LoanApprovalProcess" type="activebpel">
      <tes:wsdl>LoanService.wsdl</tes:wsdl>
      <tes:property name="BPRFile">LoanApprovalDoc.bpr</tes:property>
    </tes:put>
    <tes:partner name="assessor" wsdl="AssessorService.wsdl"/>
    <tes:partner name="approver" wsdl="ApprovalService.wsdl"/>
  </tes:deployment>

  <tes:testCases>
    <tes:testCase name="MainTemplate" basedOn="" abstract="false" vary="false">
      <tes:setUp>
        <tes:script>
          #set( $integer = 0 )
        </tes:script>
        <tes:dataSource type="velocity" src="data.vm">
          <tes:property name="iteratedVars">
            req_amount ap_reply ap_limit as_reply as_limit accepted
          </tes:property>
        </tes:dataSource>
      </tes:setUp>

      <tes:clientTrack>
        <tes:sendReceive
            service="sp:LoanServiceService"
            port="LoanServicePort"
            operation="grantLoan">
          <tes:send fault="false">
            <tes:template>
              <esq:ApprovalRequest>
                <esq:amount>$req_amount</esq:amount>
              </esq:ApprovalRequest>
            </tes:template>
          </tes:send>
          <tes:receive fault="false">
            <tes:condition>
              <tes:expression>esq:ApprovalResponse/esq:accept</tes:expression>
              <tes:value>$accepted</tes:value>
            </tes:condition>
          </tes:receive>
        </tes:sendReceive>
      </tes:clientTrack>

      <tes:partnerTrack name="approver">
        <tes:receiveSend
            service="ap:ApprovalServiceService"
            port="ApprovalServicePort"
            operation="approveLoan"
            assume="$ap_reply != 'silent'">
          <tes:send fault="false">
            <tes:template>
              <esq:ApprovalResponse>
#set( $amount = $integer.parseInt($xpath.evaluateAsString("//esq:amount", $request)) )
#if( $ap_reply != 'smart' )
                <esq:accept>$ap_reply</esq:accept>
#elseif( $ap_limit > $amount )
                <esq:accept>true</esq:accept>
#else
                <esq:accept>false</esq:accept>
#end
              </esq:ApprovalResponse>
            </tes:template>
          </tes:send>

          <tes:receive fault="false">
            <tes:condition>
              <tes:expression>//esq:amount</tes:expression>
              <tes:value>$req_amount</tes:value>
            </tes:condition>
          </tes:receive>
        </tes:receiveSend>
      </tes:partnerTrack>

      <tes:partnerTrack name="assessor" assume="$as_reply != 'silent'">
        <tes:receiveSend
            service="as:AssessorServiceService"
            port="AssessorServicePort"
            operation="assessLoan">

          <tes:receive fault="false">
            <tes:condition>
              <tes:expression>//esq:amount</tes:expression>
              <tes:value>$req_amount</tes:value>
            </tes:condition>
          </tes:receive>

          <tes:send fault="false">
            <tes:template>
              <esq:AssessorResponse>
#set( $amount = $integer.parseInt($xpath.evaluateAsString("//esq:amount", $request)) )
#if( $as_reply != 'smart' )
                <esq:risk>$as_reply</esq:risk>
#elseif( $as_limit > $amount )
                <esq:risk>low</esq:risk>
#else
                <esq:risk>high</esq:risk>
#end
              </esq:AssessorResponse>
            </tes:template>
          </tes:send>
        </tes:receiveSend>
      </tes:partnerTrack>

    </tes:testCase>
  </tes:testCases>
</tes:testSuite>
