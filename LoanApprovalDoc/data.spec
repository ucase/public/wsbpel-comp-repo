typedef int (min=0, max = 200000) Quantity;
typedef string (values={"true", "false", "silent", "smart"}) ApReply;
typedef string (values={"low", "high", "silent", "smart"}) AsReply;
typedef string (values={"true", "false"}) Accepted;

Quantity req_amount;
ApReply ap_reply;
Quantity ap_limit;
AsReply as_reply;
Quantity as_limit;
Accepted accepted;
