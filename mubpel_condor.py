#!/usr/bin/python3

import argparse
import os
import shutil
import subprocess

from glob import glob
from os.path import join as join_path, basename
from string import Template

CONDOR_JOB="mubpel.condor"
CONDOR_DAG=CONDOR_JOB + ".dag"

# Base template for all MuBPEL jobs
BASE_TEMPLATE="""
universe   = java
executable = mubpel.jar
jar_files  = mubpel.jar
java_vm_args = -Djava.io.tmpdir=/scratch_local -Dfile.encoding=UTF-8

output = mubpel.$gid.output
error  = mubpel.$gid.error

# The log is shared by all jobs, to avoid submission failures due to
# too many open files.
log    = mubpel.condor.log

# At least 1GiB of RAM available.  We need to explicitly set the
# memory requirements so Condor will not try to add them automatically
# (as it's not very good at it and asks for too much memory).
requirements = Memory > 1024

# Job execution time is also limited to a certain number of seconds
# See http://research.cs.wisc.edu/htcondor/manual/v7.8/11_Appendix_A.html#86257
timeout_seconds = $job_timeout
periodic_remove = (RemoteWallClockTime - CumulativeSuspensionTime) > $$(timeout_seconds)

# We only want the notification from DAGman saying that all jobs are done
notify_user = $email
notification = Error
"""

def prepare_condor(bpts, bpel, mutants, deps, basedir, jar,
                   job_size, email, mainclass, original,
                   test_timeout_secs, job_timeout_secs, retries, loglevel):
    if not bpts.endswith(".bpts"):
        raise Exception("The BPELUnit test suite should have a .bpts extension")
    if not bpel.endswith(".bpel"):
        raise Exception("The BPEL composition should have a .bpel extension")

    # Create the directory and copy all the dependencies
    shutil.rmtree(basedir, ignore_errors=True)
    os.mkdir(basedir)
    for f in [bpts, bpel] + deps:
        shutil.copy(f, basedir)
    shutil.copy(jar, basedir)

    # Generate all mutants or copy the mutants specified by the user
    if not mutants:
        subprocess.call(
            args=["java", "-jar", jar, "applyall", basename(bpel)],
            cwd=basedir)
        mutants = glob(join_path(basedir, "m*-*-*.bpel"))
    else:
        for mutant in mutants:
            shutil.copy(mutant, basedir)

    # Group the mutants according to job size and reduce their paths
    # to their basename (they should now be in the base directory)
    groups = []
    for job_start in range(0, len(mutants), job_size):
        groups.append([basename(m) for m in mutants[job_start : job_start + job_size]])

    # Generate the Condor job files: it's better to use separate
    # .condor files for each job, so we can use the .rescue submit
    # file if something goes wrong
    original_job_template = Template(BASE_TEMPLATE + """
arguments = "$mainclass run --mockup-port=400$$$$(VirtualMachineID) --engine-port=410$$$$(VirtualMachineID) --test-timeout=$test_timeout --logfile-level $loglevel --logfile mubpel.$gid.log $args"
queue
""".strip())
    compare_job_template = Template(BASE_TEMPLATE + """
arguments = "$mainclass compare --mockup-port=400$$$$(VirtualMachineID) --engine-port=410$$$$(VirtualMachineID) -k --test-timeout=$test_timeout --logfile-level $loglevel --logfile mubpel.$gid.log $args"
queue
""".strip())
    def generate_job(template, gid, args):
        return template.substitute(
            args=" ".join("'" + a + "'" for a in args),
            email=email,
            gid=gid,
            mainclass=mainclass,
            loglevel=loglevel,
            # MuBPEL uses milliseconds for the timeout
            test_timeout=test_timeout_secs * 1000,
            # Condor uses seconds for RemoteWallClockTime
            job_timeout=job_timeout_secs)

    original_job = "mubpel.original.condor"
    original = "mubpel.original.output"
    original_id = "ORIG"
    with open(join_path(basedir, original_job), "w") as f_job:
        f_job.write(generate_job(original_job_template, "original",
                                 (basename(bpts), basename(bpel))))

    compare_jobs = []
    for gid, group in enumerate(groups):
        args = [basename(bpts), basename(bpel), original] + group
        condor_job = "mubpel.{0:d}.condor".format(gid)
        compare_jobs.append(condor_job)

        with open(join_path(basedir, condor_job), "w") as f_job:
            f_job.write(generate_job(compare_job_template, gid, args))

    # Generate the DAGMan .dag file
    with open(join_path(basedir, CONDOR_DAG), "w") as f_dag:
        f_dag.write("JOB {0} {1}\nRETRY {0} {2}\n"
                    .format(original_id, original_job, retries))

        for jid, condor_job in enumerate(compare_jobs):
            f_dag.write("JOB {0} {1}\nRETRY {0} {2}\n"
                        .format("M" + str(jid), condor_job, retries))

        f_dag.write("PARENT {0} CHILD {1}\n"
                    .format(original_id,
                            " ".join("M" + str(jid) for jid in range(len(compare_jobs)))))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Prepare a BPEL composition for mass mutant " +
        " execution on a Condor queue. If all the r* options are" +
        " set, it will also rsync the folder into a" +
        " machine, submit the job and collect its results.")
    parser.add_argument("bpts", help="Path to the BPELUnit test suite")
    parser.add_argument("bpel", help="Path to the WS-BPEL process definition")
    parser.add_argument(
        "extra", nargs="*",
        help="Additional dependencies required by the .bpts or the .bpel")
    parser.add_argument(
        "--mutants", nargs="*",
        help="Paths to the mutants which should be run (instead of generating and running all mutants)")

    parser.add_argument(
        "--basedir", default="condor",
        help="Basename of the directory for the Condor files (without trailing slash)")
    parser.add_argument(
        "--jar",
        default=join_path(join_path(os.getenv("HOME"), "bin"), "mubpel.jar"),
        help="Path to the MuBPEL .jar file")
    parser.add_argument(
        "--jobsize", metavar="N", type=int, default=1,
        help="Number of mutants to be run per Condor job (1 by default)")
    parser.add_argument(
        "--email", default="antonio.garciadominguez@uca.es",
        help="Email address to which Condor should send emails")
    parser.add_argument(
        "--mainclass", metavar="C", default="es.uca.webservices.mutants.CLIRunner",
        help="Fully qualified name of the MuBPEL main class")
    parser.add_argument(
        "--original", default="original.xml",
        help="Basename of the file which will store the original process' output (original.xml by default)")
    parser.add_argument(
        "--test-timeout", metavar="s", type=int, default=100,
        help="BPELUnit test case timeout in seconds (100 seconds by default)")
    parser.add_argument(
        "--job-timeout", metavar="s", type=int, default=6 * 60 * 60,
        help="Condor job timeout in seconds (6 hours by default)")
    parser.add_argument(
        "--retries", metavar="N", type=int, default=6,
        help="Retry failed jobs N times (6 by default)")
    parser.add_argument(
        "--loglevel", default="ERROR",
        help=("Logging level for each MuBPEL process (ERROR by default, can be "
              + " TRACE | DEBUG | INFO | WARN | ERROR | FATAL)"))

    parser.add_argument("--ruser", help="Remote user at the Condor submission machine")
    parser.add_argument("--rhost", help="Remote host at the Condor submission machine")
    parser.add_argument("--rdir", help="Remote directory at the Condor submission machine")
    parser.add_argument(
        "--wait", action="store_true",
        help="If set together with the r* options, the script will wait for " +
             " the job to complete and collect its results")

    args = parser.parse_args()

    prepare_condor(bpts=args.bpts, bpel=args.bpel, deps=args.extra,
                   basedir=args.basedir, jar=args.jar, job_size=args.jobsize,
                   email=args.email,
                   mainclass=args.mainclass,
                   original=args.original, test_timeout_secs=args.test_timeout,
                   mutants=args.mutants, retries=args.retries,
                   loglevel=args.loglevel, job_timeout_secs=args.job_timeout)

    if args.ruser and args.rhost and args.rdir:
        subprocess.check_call(
            ["rsync", "-cravz", "--delete", "--delete-excluded", "--exclude", "'*.log'",
             args.basedir + "/",
             "{}@{}:{}".format(args.ruser, args.rhost, args.rdir)])

        s_template = " && ".join((
            "cd '$rdir'",
            "condor_submit_dag -no_submit -notification Always '$dag' >&2",
            "sed -i 's/^queue$$/notify_user = $email\\nqueue/' '$dag.condor.sub'",
            "condor_submit '$dag.condor.sub' >&2"))
        if args.wait:
            s_template = " && ".join((
                s_template,
                "condor_wait '$dag.dagman.log' >&2",
                "(ls -1 | egrep '^mubpel[.][0-9]+[.]output$$' | (while read f; do cat \"$$f\"; done) | sort)"))

        cmd = Template(s_template).substitute(rdir=args.rdir, email=args.email,
                                              dag=CONDOR_DAG, job=CONDOR_JOB)

        subprocess.check_call(["ssh", "{}@{}".format(args.ruser, args.rhost), cmd])
